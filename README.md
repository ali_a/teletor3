# teletor3

Scripts to receive tor bridges over email and sending them to a Telegram channel.

# TODO

[ x ] phase 1: send bridge request

[ x ] phase 2: receive all new mails

[ x ] phase 3: extract bridges from emails

[ x ] phase 4: make sure bridges are unique

[ x ] phase 5: send to telegram + paste service

[ x ] phase 6: bash scripts

[ x ] other: MarkDown doesn't work. make it work!

[ ] readme: make readme great again!