import smtplib
from time import sleep
import email
import imaplib
import os


def send_mail(username, password, msg):
    """Send an email to BridgesDB."""
    BridgesDB = 'bridges@bridges.torproject.org'
    gmail_smtp_server = 'smtp.gmail.com:587'
    print(f'sending email via: {username}')
    server = smtplib.SMTP(gmail_smtp_server)
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login(username, password)
    server.sendmail(from_addr=username, to_addrs=BridgesDB, msg=msg)
    server.quit()
    print('sleeping for 5 seconds...')
    sleep(5)


def _save_mail(email_data, uid, username, directory):
    """
    Save the given mail as plain text.

    remove headers and attachments of the mail and only keeps the plain text.
    Return: name of the file containing the plain text.
    """
    # email_data is a HUGE piece of mess and I don't understand its structure
    # so here we are following the instructions on internet!

    # fetch the email body (RFC822) for the given email_date
    raw_email = email_data[0][1]
    raw_email_string = raw_email.decode('utf-8')
    email_message = email.message_from_string(raw_email_string)

    # this will loop through all the available multiparts in mail.
    # we only care about its plain text. other parts are ignored.
    for part in email_message.walk():
        # ignore attachments/html
        if part.get_content_type() == "text/plain":
            body = part.get_payload(decode=True)
            save_string = f'{username}-{uid}.email'
            full_path = os.path.join(directory, save_string)

            # location on disk
            my_file = open(full_path, 'w')
            my_file.write(body.decode('utf-8'))
            my_file.close()

            print(f'Mail saved as: {full_path}')
            return


def receive_all_mails_for_user(username, password, directory):
    """
    Receive and save all the new mails belonging to the user.

    delete mails after saving them.
    """
    # with the help of:
    # http://www.vineetdhanawat.com/blog/2012/06/how-to-extract-email-gmail-\
    # contents-as-text-using-imaplib-via-imap-in-python-3/

    gmail_imap_server = 'imap.gmail.com'
    mail = imaplib.IMAP4_SSL(gmail_imap_server)

    # mail.login() will return a tuple including the logging result and the
    # email we logged to.
    # we will ignore that email address as we already know it
    result, _ = mail.login(username, password)
    assert (result == 'OK'), f'Cannot log into {username}'
    print(f'logged in: {username}')

    # connecting to inbox. inbox is one of many lables we can access.
    result, _ = mail.select('inbox')
    assert (result == 'OK'), f'Cannot access inbox for {username}'
    print(f'inbox access granted for {username}')

    # search inbox for all available emails.
    # data is a list that contains a byte literal. this byte literal
    # contains our email unique ids in a space-separated format
    # for instance: [b'72 73 74 75 76']
    # so an instance of ids would be like: [b'72', b'73', b'74', b'75', b'76']
    result, data = mail.uid('search', None, "ALL")
    assert (result == 'OK'), f'Cannot search the inbox for {username}'
    assert (isinstance(data, list)), \
        f'data was expected to be a list, got {type(data)}'
    ids = data[0].split()

    if ids == []:
        print(f'No new mail for {username}')
        return

    # fetch the mail in RFC822 format.
    # save all the mails in inbox as plain text and then, delete them
    for uid in ids:
        result, email_data = mail.uid('fetch', uid, '(RFC822)')
        assert (result == 'OK'), f'Cannot fetch email uid: {uid}'
        _save_mail(email_data, uid, username, directory)

        # delete mail permanently
        result, _ = mail.uid('STORE', uid, '+FLAGS', '(\Deleted)')
        if result == 'OK':
            print(f'Deleted {uid} from server.')
        else:
            print(f'Failed to delete {uid} from server!')

    mail.close()
