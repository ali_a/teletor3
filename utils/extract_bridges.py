"""Extract bridges from stored files."""
import os
import re
from time import strftime


def _extract_bridges(email_string, regex):
    """
    Extract bridges from the given email_string.

    Args:
        email_string: a list that each of its items is a plain text email

    Returns: a list of bridges
    """
    bridges = []

    for email in email_string:
        search_result = re.findall(regex, email)
        if search_result is not None:
            bridges.extend(search_result)

    return bridges


def _save_bridges(bridge_list, base_dir):
    """Save the given list or set."""
    string = ''
    for bridge in bridge_list:
        string += bridge + '\n\n'

    file_name = str(strftime('%Y-%m-%d-%H-%M-%S'))
    file_path = os.path.join(base_dir, file_name)
    with open(file_path, 'w') as file:
        file.write(string)
    print(f'saved as :{file_path}')


def get_only_file(path):
    """
    Get regular files (and not dirs) in the given path.

    files in sub-directories are ignored
    """
    return [file for file in os.listdir(path)
            if os.path.isfile(os.path.join(path, file))]


def _mark_as_read(all_files, new_emails_dir, read_emails_dir):
    """Move email files to read directory."""
    for content_file in all_files:
        source = os.path.join(new_emails_dir, content_file)
        destination = os.path.join(read_emails_dir, content_file)
        os.rename(source, destination)
    print('All mails in emails dir marked as read.')


def extract_from_directory(directories: dict, regex):
    new_emails_dir = directories['new_emails']
    read_emails_dir = directories['read_emails']
    base_dir = directories['base']

    """Extract all bridges from stored files and save them."""
    all_files = get_only_file(new_emails_dir)
    full_path_files = [os.path.join(new_emails_dir, file)
                       for file in all_files]

    if all_files == []:
        print('No mail in emails dir. returning.')
        return

    # a list which we append our email plain texts to it.
    # we will extract bridges from this list later.
    email_string = []

    # loop over all the files and append its content to email_string
    for file in full_path_files:
        with open(file, 'r') as reader:
            email_string.append(reader.read())

    # extract bridge from list using a regex
    bridges = _extract_bridges(email_string, regex)

    if bridges == []:
        print('No bridges to extract. returning.')
        _mark_as_read(all_files, new_emails_dir, read_emails_dir)
        return

    # remove duplicated items
    bridges_unique = set(bridges)

    # let's show off the number of bridges we received :)
    print(f'Extracted {len(bridges_unique)} Bridges.')

    # save bridges_unique as a unified text in VPN directory
    _save_bridges(bridges_unique, base_dir)

    # mark them as read so we won't be dealing with them in future attempts
    _mark_as_read(all_files, new_emails_dir, read_emails_dir)
