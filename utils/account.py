from .email_exchange import send_mail
from .email_exchange import receive_all_mails_for_user as receive_all


class Account:
    def __init__(self, username, password, msg, directories: dict):
        self.username = username
        self.password = password
        self.msg = msg

        self.directories = directories
        self.base_dir = directories['base']
        self.new_emails_dir = directories['new_emails']
        self.read_emails_dir = directories['read_emails']
        self.sent_dir = directories['sent']

    def send(self):
        send_mail(self.username, self.password, self.msg)

    def receive(self):
        receive_all(self.username, self.password, self.new_emails_dir)
