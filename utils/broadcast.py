"""Module to send bridges to a telegram channel."""

from requests import post
from pyrogram import Client, ParseMode
from .extract_bridges import get_only_file
import os
from . import config


def _paste(text):
    """
    Paste the text on a paste service and return its address.

    return a empty string if failed.
    """
    # ubuntu.ir paste is pretty easy to use. no captcha or complicated steps.

    paste_url = 'https://paste.ubuntu.ir'
    data = {'text': text}
    response_to_post_request = post(paste_url, data=data)
    response = response_to_post_request.request
    if(response_to_post_request.status_code == 200):
        return response.url
    return ''


def send_to_telegram(base_dir, sent_dir, channel_id):
    """Search VPN dir and send the oldest file to Telegram channel."""
    # find the oldest file
    dir_content = get_only_file(base_dir)
    if dir_content == []:
        print('No files in VPN directory. returning.')
        return
    dir_content = sorted(dir_content)
    oldest_file_name = dir_content[0]
    oldest_file_path = os.path.join(base_dir, oldest_file_name)

    with open(oldest_file_path, 'r') as reader:
        bridges = reader.read()

    # let's try to paste our bridges on a paste service
    paste_url = _paste(bridges)

    # there are 2 new lines (\n) at the end of bridges text and
    # it will cause the next step to cause a problem (not being detected as
    # a piece of code in Telegram). So we will drop the 2 new lines.
    bridges = bridges[:-2]

    # add triple ` to the beginning and end of the bridges
    # this will cause bridges to be in code style when sent to
    # Telegram in MarkDown style
    bridges = f'```{bridges}```'

    # add a timestamp at the top of message followed by the paste url
    # two point here:
    # 1.name of the file is a timestamp so we will use it
    #   we will use first 10 characters (YYYY-MM-DD) and omit the rest
    # 2.the paste url will be a blank string in case of failure in pasting
    #   so it is safe to append it to the final content
    final_content = oldest_file_name[:10] + ': '
    final_content += paste_url + '\n\n'
    final_content += bridges

    # now let's send our final_content to telegram channel
    app = Client(config.bot_session_name)
    with app:
        app.send_message(channel_id, final_content,
                         parse_mode=ParseMode.MARKDOWN)
        print('Content sent to Telegram.')

    # mark oldest file as sent
    source = oldest_file_path
    destination = os.path.join(sent_dir, oldest_file_name)
    os.rename(source, destination)
    print(f'Marked as sent: {destination}')
