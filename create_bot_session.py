"""A simple module to create a Telegram session for our bot."""

from config import bot_token, bot_session_name
from pyrogram import Client
from os.path import exists

if exists(bot_session_name + '.session'):
    print('session already exists. exiting.')
    exit(1)

app = Client(bot_session_name, bot_token=bot_token)
app.start()
app.stop()
print('session created.')
