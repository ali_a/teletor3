#!/bin/bash
teletor_dir='/home/ali/Projects/teletor_3/'
venv=$teletor_dir'venv/bin/activate'

echo '*** Teletor Bash Service (TBS) ***'

cd $teletor_dir
echo $TBS_Header 'working directory :' $(pwd)

source $venv
echo $TBS_Header 'using python:' $(which python)

python run.py
