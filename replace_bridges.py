from utils import config
import os

bridges_to_read = 15
path = config.obfs4_sent

def read_all_bridges(path_to_file):
    with open(path_to_file, 'r') as f:
        file_content = f.read().strip().split('\n\n')
    return file_content

raw_file_names = sorted(os.listdir(path), reverse=True)
raw_file_names = [os.path.join(path, p) for p in raw_file_names]

# extract bridges from files
bridges = []
for file_name in raw_file_names:
    content = read_all_bridges(file_name)
    bridges.extend(content)
    if len(bridges) >= bridges_to_read:
        break

# remove duplicated bridges
bridges = set(bridges)

# create a content to write on /etc/tor/torrc
content_to_write = 'UseBridges 1\nClientTransportPlugin obfs4 exec /usr/bin/obfs4proxy\n'
for bridge in bridges:
    content_to_write += 'Bridge ' + bridge + '\n'

with open('/etc/tor/torrc', 'w') as torrc:
    torrc.write(content_to_write)

print('Done.')