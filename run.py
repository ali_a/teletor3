from utils import account
from utils import config
from utils import extract_bridges
from utils import broadcast

# init email accounts
obfs1 = account.Account(**config.obfs4_1)
obfs2 = account.Account(**config.obfs4_2)
obfs3 = account.Account(**config.obfs4_3)
obfs_accounts = [obfs1, obfs2, obfs3]

vanilla1 = account.Account(**config.vanilla_1)
vanilla_accounts = [vanilla1]

all_accounts = [obfs1, obfs2, obfs3, vanilla1]


def send_all():
    for acc in all_accounts:
        acc.send()


def recieve_all():
    for acc in all_accounts:
        acc.receive()


def extract_all():
    extract_bridges.extract_from_directory(directories=config.obfs4_dirs,
                                           regex=config.obfs4_regex)
    extract_bridges.extract_from_directory(directories=config.vanilla_dirs,
                                           regex=config.vanilla_regex)


def broadcast_all():
    broadcast.send_to_telegram(base_dir=config.obfs4_base_dir,
                               sent_dir=config.obfs4_sent,
                               channel_id=config.obfs_channel)
    broadcast.send_to_telegram(base_dir=config.vanilla_base_dir,
                               sent_dir=config.vanilla_sent,
                               channel_id=config.vanilla_channel)


if __name__ == "__main__":
    send_all()
    recieve_all()
    extract_all()
    broadcast_all()
